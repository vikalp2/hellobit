import React from 'react';

export type ButtonProps = {
  /**
   * a button to be rendered with given label.
   */
  children?: string;
};

export function Button({ children }: ButtonProps) {
  return <button>{children}</button>;
}
